const color = {
  primary: "#00D1E3",
  darkblue: "#151935",
  lightblue: "#D8EAFF",
  lightgrey: "#E9E5E5",
  blue: "#5A618D",
  green: "#3bdb68",
  red: "#BD1A01",
  orange: "#FF9C40",
  darkgrey: "#3C3939",
  white: "#fff",
  yellow: "#F9C834",
  grey: "#B7B7B7",
  black: "#000",
};

module.exports = {
  mode: "jit",
  purge: [
    "./components/**/*.{vue,js}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      kanit: ["Kanit", "sans-serif"],
      roboto: ["Roboto", "sans-serif"],
    },
    screens: {
      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
    },
    textColor: color,
    backgroundColor: color,
    borderColor: color,
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
