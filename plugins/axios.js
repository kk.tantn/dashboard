export default function ({ $axios }, inject) {
  const appApi = $axios.create({
    baseURL: `${process.env.api_url}/api`,
    // withCredentials: true,
  });

  appApi.onResponseError((error) => onResponseError(error));
  appApi.onRequest((config) => onRequest(config));

  const onResponseError = (error) => {
    // console.log('error => ', error)
    // console.log('---------------------')
  };

  const onRequest = (config) => {
    // const access_token = $cookies.get("outline_token");
    // if (!access_token) return;
    // config.headers.common["Authorization"] = access_token;
  };

  inject("appApi", appApi);
}
